FROM nginx:latest
MAINTAINER OPENFANS

RUN sed -i 's/deb.debian.org/mirrors.ustc.edu.cn/g' /etc/apt/sources.list ; sed -i 's/security.debian.org/mirrors.ustc.edu.cn/g' /etc/apt/sources.list ; apt-get update &&  apt-get install -y supervisor && apt-get clean && rm -rf /var/cache/apt/archives/*

ADD supervisord.conf /etc/supervisor/conf.d/supervisord.conf

COPY . /app/

RUN mkdir /etc/nginx/templating ; mkdir -p /etc/nginx/certs

ADD sha1check.sh /usr/local/bin/nginx-reload
ADD domain_bind /usr/local/bin/domain_bind
ADD default.conf /etc/nginx/conf.d/default.conf
ADD conf.tmpl /etc/nginx/templating/conf.tmpl
ADD cecos.key /etc/nginx/certs/cecos.key
ADD cecos.crt /etc/nginx/certs/cecos.crt


RUN chmod u+x /usr/local/bin/nginx-reload
RUN chmod u+x /usr/local/bin/domain_bind

WORKDIR /app/

RUN chown nginx /var/log/nginx/*

ENV DOCKER_HOST unix:///tmp/docker.sock

VOLUME ["/etc/nginx/certs"]

ENTRYPOINT ["/usr/bin/supervisord"]
