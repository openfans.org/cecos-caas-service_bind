# CecOS CaaS
----
# Service(Domain)_Bind

service(domain)_bind is a nginx configuration generator that using **docker swarm** , service(domain)_bind watch the docker swarm event for service created and then generating the nginx configuration automatically

## How to Compile

```bash
CGO_ENABLED=0 go build -o domain_bind service_bind.go nginx-conf.go
```

## How to Run

By default **service(domain)_bind** will set the endpoint of docker socket to **/tmp/docker.sock** but can be adjustable using the parameter

```bash
Usage of service(domain)_bind:
  -endpoint string
        Docker Endpoint (socket only) (default "/tmp/docker.sock")
```

## How to Use

### 1. Non-Docker Environment

Just execute the **service(domain)_bind** using the -endpoint parameter 

```bash
domain_bind -endpoint /var/run/docker.sock
```

### 2. Docker Environment

- Build this **service(domain)_bind** image

```bash
docker build -t openfans/service_bind:latest .
```

- Make sure the docker is swarm enabled

- Create the swarm Overlay Network first

```bash
docker network create --driver overlay --subnet 172.18.188.0/24 \
domain_bind-backend --attachable
```

 OR User system defined network subnet.

```bash
docker network create --driver overlay domain_bind --attachable
```

- Make sure the **service(domain)_bind** container run on **manager** node 

```bash
docker service create --network domain_bind --name domain_bind \
--constraint 'node.role == manager' --publish 80:80 --publish 443:443 \
--mount type=bind,src=/var/run/docker.sock,dst=/tmp/docker.sock,ro=true \
--mount type=bind,src=/opt/ssl,dst=/etc/nginx/certs openfans/service_bind:latest
```

OR Use default Cert: cecos.crt & cecos.key

```bash
docker service create --network domain_bind --name domain_bind \
--constraint 'node.role == manager' --publish 80:80 --publish 443:443 \
--mount type=bind,src=/var/run/docker.sock,dst=/tmp/docker.sock,ro=true \
openfans/service_bind:latest
```


## Other Service Creating

If there are new service(s) created on swarm, **service(domain)_bind** will watch it and update the nginx configuration and reload it based on the service labels:

- example running nginx-web app (with SSL)
```bash
docker service create --label service.domain=https.example.com \
--label service.port=80 --label service.ssl=true \
--label service.ssl.cert=cecos.crt --label service.ssl.key=cecos.key \
--network domain_bind --name https-example nginx:latest
```

- example running nginx-web app (non SSL)
```bash
docker service create --label service.domain=http.example.com \
--label service.port=80 --network domain_bind \
--name http-example nginx:latest
```

**Labels Description :**
- **service.domain** ``set the domain name``
- **service.port** ``set the exposed container port``
- **service.ssl** ``set to true if using ssl``
- **service.ssl.cert** ``name of certificate``
- **service.ssl.key** ``name of the certificate key``


